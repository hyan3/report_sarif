package report

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewRef(t *testing.T) {
	vuln := Vulnerability{
		CompareKey: "debian:10:nghttp2:CVE-2019-9511",
	}

	want := Ref{
		ID:         "6ffc7fa990af5bf01f0d5a4aed1b84e924dd64e6fd7e0eb6fad862e9b66d5cd2",
		CompareKey: "debian:10:nghttp2:CVE-2019-9511",
	}

	got := NewRef(vuln)
	require.Equal(t, want, got)
}
