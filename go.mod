module gitlab.com/hyan3/report_sarif

go 1.13

require (
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.8.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.1
	gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2 v2.0.2
)
