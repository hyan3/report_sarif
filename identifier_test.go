package report

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIdentifier(t *testing.T) {
	var tcs = []struct {
		Name  string
		Parse string // argument given to ParseIdentifier
		Got   Identifier
		Want  Identifier
	}{
		{
			Name:  "CVEIdentifier",
			Parse: "CVE-123",
			Got:   CVEIdentifier("CVE-123"),
			Want: Identifier{
				Type:  "cve",
				Name:  "CVE-123",
				Value: "CVE-123",
				URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-123",
			},
		},
		{
			Name:  "CWEIdentifier",
			Parse: "CWE-123",
			Got:   CWEIdentifier(123),
			Want: Identifier{
				Type:  "cwe",
				Name:  "CWE-123",
				Value: "123",
				URL:   "https://cwe.mitre.org/data/definitions/123.html",
			},
		},
		{
			Name:  "OSVDBIdentifier",
			Parse: "OSVDB-123",
			Got:   OSVDBIdentifier("OSVDB-123"),
			Want: Identifier{
				Type:  "osvdb",
				Name:  "OSVDB-123",
				Value: "OSVDB-123",
				URL:   "https://cve.mitre.org/data/refs/refmap/source-OSVDB.html",
			},
		},
		{
			Name:  "USNIdentifier",
			Parse: "USN-123",
			Got:   USNIdentifier("USN-123"),
			Want: Identifier{
				Type:  "usn",
				Name:  "USN-123",
				Value: "USN-123",
				URL:   "https://usn.ubuntu.com/123/",
			},
		},
		{
			Name:  "RHSAIdentifier",
			Parse: "RHSA-2019:3892",
			Got:   RHSAIdentifier("RHSA-2019:3892"),
			Want: Identifier{
				Type:  "rhsa",
				Name:  "RHSA-2019:3892",
				Value: "RHSA-2019:3892",
				URL:   "https://access.redhat.com/errata/RHSA-2019:3892",
			},
		},
		{
			Name:  "GHSAIdentifier",
			Parse: "GHSA-w64w-qqph-5gxm",
			Got:   GHSAIdentifier("GHSA-w64w-qqph-5gxm"),
			Want: Identifier{
				Type:  "ghsa",
				Name:  "GHSA-w64w-qqph-5gxm",
				Value: "GHSA-w64w-qqph-5gxm",
				URL:   "https://github.com/advisories/GHSA-w64w-qqph-5gxm",
			},
		},
		{
			Name:  "ELSAIdentifier",
			Parse: "ELSA-2017-1101",
			Got:   ELSAIdentifier("ELSA-2017-1101"),
			Want: Identifier{
				Type:  "elsa",
				Name:  "ELSA-2017-1101",
				Value: "ELSA-2017-1101",
				URL:   "https://linux.oracle.com/errata/ELSA-2017-1101.html",
			},
		},
		{
			Name:  "H1Identifier",
			Parse: "HACKERONE-350401",
			Got:   H1Identifier("HACKERONE-350401"),
			Want: Identifier{
				Type:  "hackerone",
				Name:  "HACKERONE-350401",
				Value: "350401",
				URL:   "https://hackerone.com/reports/350401",
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			require.Equal(t, tc.Want, tc.Got)
		})
	}

	t.Run("ParseIdentifier", func(t *testing.T) {
		for _, tc := range tcs {
			got, ok := ParseIdentifierID(tc.Parse)
			require.Equalf(t, ok, true, "Expected %s to be parsed successfully", tc.Parse)
			require.Equal(t, tc.Want, got)
		}
	})
}
