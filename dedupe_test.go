package report

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDedupe(t *testing.T) {

	// nokogiri CVE-2018-14404, USN-3739-1 via app/Gemfile.lock (dup)
	var vuln = Vulnerability{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: &Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// nokogiri CVE-2018-14404 via app/Gemfile.lock (dup)
	var vulnDup = Vulnerability{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: &Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// nokogiri USN-3739-1 via app/Gemfile.lock (dup)
	var vulnDup2 = Vulnerability{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: &Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
		},
	}

	// nokogiri CVE-2018-14404, USN-3739-1 via other/Gemfile.lock
	var vulnOtherFile = Vulnerability{
		Location: Location{
			File: "other/Gemfile.lock",
			Dependency: &Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// libxml CVE-2018-14404, USN-3739-1 via app/Gemfile.lock
	var vulnOtherPackage = Vulnerability{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: &Dependency{
				Package: Package{
					Name: "libxml",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3739-1"),
			CVEIdentifier("CVE-2018-14404"),
		},
	}

	// nokogiri USN-3271-1 via app/Gemfile.lock
	var vulnOtherID = Vulnerability{
		Location: Location{
			File: "app/Gemfile.lock",
			Dependency: &Dependency{
				Package: Package{
					Name: "nokogiri",
				},
			},
		},
		Identifiers: []Identifier{
			USNIdentifier("USN-3271-1"),
		},
	}

	var input = []Vulnerability{
		vuln,
		vulnOtherFile,
		vulnDup,
		vulnDup2,
		vulnOtherPackage,
		vulnOtherID,
	}

	var want = []Vulnerability{
		vuln,
		vulnOtherFile,
		vulnOtherPackage,
		vulnOtherID,
	}

	got := Dedupe(input...)
	require.Equal(t, want, got)
}
