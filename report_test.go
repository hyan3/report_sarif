package report

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

const expectedTime = "2020-01-25T15:04:05"

func testReport() Report {
	parsedStartTime, _ := time.Parse(timeFormat, expectedTime)
	parsedEndTime, _ := time.Parse(timeFormat, expectedTime)

	startTime := ScanTime(parsedStartTime)
	endTime := ScanTime(parsedEndTime)

	return Report{
		Version: Version{14, 0, 3, ""},
		Vulnerabilities: []Vulnerability{
			{
				Category:   CategoryDependencyScanning,
				Name:       "Vulnerability in io.netty/netty",
				Message:    "Vulnerability in io.netty/netty",
				CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
				Scanner: &Scanner{
					ID:   "find_sec_bugs",
					Name: "Find Security Bugs",
				},
				Location: Location{
					File: "app/pom.xml",
					Dependency: &Dependency{
						Package: Package{
							Name: "io.netty/netty",
						},
						Version: "3.9.1.Final",
					},
				},
				Identifiers: []Identifier{
					CVEIdentifier("CVE-2018-1234"),
				},
			},
		},
		Remediations: []Remediation{
			{
				Fixes: []Ref{
					{
						CompareKey: "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
						ID:         "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3",
					},
				},
				Summary: "Upgrade to netty 3.9.2.Final",
				Diff:    "diff (base64 encoded) placeholder",
			},
		},
		DependencyFiles: []DependencyFile{
			{
				Path:           "app/pom.xml",
				PackageManager: PackageManagerMaven,
				Dependencies: []Dependency{
					{
						IID:     1,
						Direct:  true,
						Package: Package{Name: "io.netty/netty"},
						Version: "3.9.2.Final",
					},
					{
						IID:     2,
						Direct:  true,
						Package: Package{Name: "org.powermock/powermock-api-mockito"},
						Version: "1.7.3",
					},
					{
						IID:            3,
						DependencyPath: []DependencyRef{{IID: 2}},
						Package:        Package{Name: "org.mockito/mockito-core"},
						Version:        "1.10.19",
					},
					{
						IID:            4,
						DependencyPath: []DependencyRef{{IID: 2}, {IID: 3}},
						Package:        Package{Name: "org.hamcrest/hamcrest-core"},
						Version:        "1.1",
					},
				},
			},
		},
		Scan: Scan{
			Analyzer: ScannerDetails{
				ID:      "spotbugs",
				Name:    "Spotbugs",
				Version: "2.1.2",
				URL:     "https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs",
				Vendor: Vendor{
					Name: "GitLab",
				},
			},
			Scanner: ScannerDetails{
				ID:      "find_sec_bugs",
				Name:    "Find Security Bugs",
				Version: "1.0.1",
				URL:     "https://github.com/find-sec-bugs/find-sec-bugs",
				Vendor: Vendor{
					Name: "OWASP",
				},
			},
			PrimaryIdentifiers: []Identifier{
				CVEIdentifier("CVE-2018-1234"),
			},
			Type:      CategoryDependencyScanning,
			StartTime: &startTime,
			EndTime:   &endTime,
			Status:    "success",
		},
	}
}

var testReportJSON = fmt.Sprintf(`{
  "version": "14.0.3",
  "vulnerabilities": [
    {
      "id": "466fba6b35cdb9bd077db9a53584822a1b5fcdf97417e5956c1ab9a1cc697c76",
      "category": "dependency_scanning",
      "name": "Vulnerability in io.netty/netty",
      "message": "Vulnerability in io.netty/netty",
      "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
      "scanner": {
        "id": "find_sec_bugs",
        "name": "Find Security Bugs"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.1.Final"
        }
      },
      "identifiers": [
        {
          "type": "cve",
          "name": "CVE-2018-1234",
          "value": "CVE-2018-1234",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
        }
      ]
    }
  ],
  "remediations": [
    {
      "fixes": [
        {
          "cve": "app/pom.xml:io.netty/netty:cve:CVE-2018-1234",
          "id": "bb2fbeb1b71ea360ce3f86f001d4e84823c3ffe1a1f7d41ba7466b14cfa953d3"
        }
      ],
      "summary": "Upgrade to netty 3.9.2.Final",
      "diff": "diff (base64 encoded) placeholder"
    }
  ],
  "dependency_files": [
    {
      "path": "app/pom.xml",
      "package_manager": "maven",
      "dependencies": [
        {
          "iid": 1,
          "direct": true,
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.2.Final"
        },
        {
          "iid": 2,
          "direct": true,
          "package": {
            "name": "org.powermock/powermock-api-mockito"
          },
          "version": "1.7.3"
        },
        {
          "iid": 3,
          "dependency_path": [
            {
              "iid": 2
            }
          ],
          "package": {
            "name": "org.mockito/mockito-core"
          },
          "version": "1.10.19"
        },
        {
          "iid": 4,
          "dependency_path": [
            {
              "iid": 2
            },
            {
              "iid": 3
            }
          ],
          "package": {
            "name": "org.hamcrest/hamcrest-core"
          },
          "version": "1.1"
        }
      ]
    }
  ],
  "scan": {
    "analyzer": {
      "id": "spotbugs",
      "name": "Spotbugs",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs",
      "vendor": {
        "name": "GitLab"
      },
      "version": "2.1.2"
    },
    "scanner": {
      "id": "find_sec_bugs",
      "name": "Find Security Bugs",
      "url": "https://github.com/find-sec-bugs/find-sec-bugs",
      "vendor": {
        "name": "OWASP"
      },
      "version": "1.0.1"
    },
    "primary_identifiers": [
      {
        "type": "cve",
        "name": "CVE-2018-1234",
        "value": "CVE-2018-1234",
        "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"
      }
    ],
    "type": "dependency_scanning",
    "start_time": "%s",
    "end_time": "%s",
    "status": "success"
  }
}`, expectedTime, expectedTime)

var testEmptySASTReport = func() Report {
	r := NewReport()
	r.DependencyFiles = nil // emulate SAST report
	return r
}()

var testEmptyReportJSON = `{
  "version": "15.0.7",
  "vulnerabilities": [],
  "dependency_files": [],
  "scan": {
    "analyzer": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "scanner": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "type": ""
  }
}`

var testEmptySASTReportJSON = `{
  "version": "15.0.7",
  "vulnerabilities": [],
  "dependency_files": null,
  "scan": {
    "analyzer": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "scanner": {
      "id": "",
      "name": "",
      "vendor": {
        "name": ""
      },
      "version": ""
    },
    "type": ""
  }
}`

func TestReport(t *testing.T) {
	testCases := []struct {
		Name string
		Report
		ReportJSON string
	}{
		{
			Name:       "GenericReport",
			Report:     testReport(),
			ReportJSON: testReportJSON,
		},
		{
			Name:       "EmptyReport",
			Report:     NewReport(),
			ReportJSON: testEmptyReportJSON,
		},
		{
			Name:       "EmptySASTReport",
			Report:     testEmptySASTReport,
			ReportJSON: testEmptySASTReportJSON,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			t.Run("MarshalJSON", func(t *testing.T) {
				b, err := json.Marshal(tc.Report)
				require.NoError(t, err)

				var buf bytes.Buffer
				json.Indent(&buf, b, "", "  ")
				got := buf.String()

				want := tc.ReportJSON
				require.JSONEq(t, want, got)
			})

			t.Run("UnmarshalJSON", func(t *testing.T) {
				var got Report
				err := json.Unmarshal([]byte(tc.ReportJSON), &got)
				require.NoError(t, err)
				require.Equal(t, tc.Report, got)
			})
		})
	}

	t.Run("ExcludePaths", func(t *testing.T) {
		report := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Vulnerability{
				{
					Name:       "R1/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/critical/ckey",
					Location:   Location{File: "api/pom.xml"},
				},
				{
					Name:       "R2/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/critical/ckey",
					Location:   Location{File: "api/pom.xml"},
				},
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
					Location:   Location{File: "web/pom.xml"},
				},
				{
					Name:       "R1/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/low/ckey",
					Location:   Location{File: "model/pom.xml"},
				},
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R1/critical/ckey"}}, // excluded
					Summary: "Upgrade dependency to fix R1/critical",
					Diff:    "diff fixing R1/critical",
				},
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "api/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "ognl/ognl"}, Version: "3.1.8"},
						{Package: Package{Name: "org.apache.struts/struts2-core"}, Version: "2.5.1"},
						{Package: Package{Name: "org.freemarker/freemarker"}, Version: "2.3.23"},
					},
				},
				{
					Path:           "model/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "org.apache.logging.log4j/log4j-api"}, Version: "2.8.2"},
						{Package: Package{Name: "org.apache.logging.log4j/log4j-core"}, Version: "2.8.2"},
					},
				},
				{
					Path:           "pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
				{
					Path:           "web/pom.xml",
					PackageManager: "bundler",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.2"},
						{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
					},
				},
			},
		}

		want := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Vulnerability{
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
					Location:   Location{File: "web/pom.xml"},
				},
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
					Location:   Location{File: "pom.xml"},
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
				{
					Path:           "web/pom.xml",
					PackageManager: "bundler",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.2"},
						{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
					},
				},
			},
		}

		isExcluded := func(path string) bool {
			switch path {
			case "api/pom.xml", "model/pom.xml":
				return true
			default:
				return false
			}
		}

		report.ExcludePaths(isExcluded)
		require.Equal(t, want, report)
	})
}

func TestNewReport(t *testing.T) {
	got := NewReport().Version
	want := CurrentVersion()
	require.Equal(t, want, got)
}

func TestSort(t *testing.T) {
	t.Run("Vulnerabilities", func(t *testing.T) {
		tcs := []struct {
			Name            string
			Vulnerabilities []Vulnerability
			Want            []Vulnerability
		}{
			{
				Name: "it sorts by SeverityLevel when all SeverityLevels differ",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelUnknown, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelHigh, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelLow, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelInfo, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelMedium, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
					{Severity: SeverityLevelHigh, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelMedium, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelLow, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelUnknown, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelInfo, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
				},
			},
			{
				Name: "it sorts by CompareKey when all SeverityLevels are the same",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelCritical, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "b", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "c", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
					{Severity: SeverityLevelCritical, CompareKey: "d", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "e", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "f", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
				},
			},
			{
				Name: "it sorts by Location.Dependency.Version when all SeverityLevel and CompareKeys are the same",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "0.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "1.2.3"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "4.0.1"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "6.4.0"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "7.8.9"}}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{Dependency: &Dependency{Version: "9.2.5"}}},
				},
			},
			{
				Name: "it does not sort by Location.Dependency.Version when the Location.Dependency field doesn't exist",
				Vulnerabilities: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "a.go", LineStart: 6}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "e.go", LineStart: 4}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "b.go", LineStart: 1}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "d.go", LineStart: 7}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "f.go", LineStart: 0}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "c.go", LineStart: 9}},
				},
				Want: []Vulnerability{
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "a.go", LineStart: 6}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "e.go", LineStart: 4}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "b.go", LineStart: 1}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "d.go", LineStart: 7}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "f.go", LineStart: 0}},
					{Severity: SeverityLevelCritical, CompareKey: "a", Location: Location{File: "c.go", LineStart: 9}},
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				report := Report{
					Vulnerabilities: tc.Vulnerabilities,
				}

				report.Sort()

				require.Equal(t, tc.Want, report.Vulnerabilities)
			})
		}
	})

	t.Run("DependencyFiles", func(t *testing.T) {
		tcs := []struct {
			Name            string
			DependencyFiles []DependencyFile
			Want            []DependencyFile
		}{
			{
				Name: "it sorts by Path",
				DependencyFiles: []DependencyFile{
					{Path: "f"},
					{Path: "b"},
					{Path: "e"},
					{Path: "a"},
					{Path: "d"},
					{Path: "c"},
				},
				Want: []DependencyFile{
					{Path: "a"},
					{Path: "b"},
					{Path: "c"},
					{Path: "d"},
					{Path: "e"},
					{Path: "f"},
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				report := Report{
					DependencyFiles: tc.DependencyFiles,
				}

				report.Sort()

				require.Equal(t, tc.Want, report.DependencyFiles)
			})
		}
	})

	t.Run("Remediations", func(t *testing.T) {
		tcs := []struct {
			Name         string
			Remediations []Remediation
			Want         []Remediation
		}{
			{
				Name: "it sorts by Fixes[0].CompareKey",
				Remediations: []Remediation{
					{Summary: "f", Fixes: []Ref{{CompareKey: "f"}, {CompareKey: "z"}}},
					{Summary: "b", Fixes: []Ref{{CompareKey: "b"}, {CompareKey: "d"}}},
					{Summary: "e", Fixes: []Ref{{CompareKey: "e"}}},
					{Summary: "a", Fixes: []Ref{{CompareKey: "a"}, {CompareKey: "l"}}},
					{Summary: "d", Fixes: []Ref{{CompareKey: "d"}}},
					{Summary: "c", Fixes: []Ref{{CompareKey: "c"}, {CompareKey: "e"}}},
				},
				Want: []Remediation{
					{Summary: "a", Fixes: []Ref{{CompareKey: "a"}, {CompareKey: "l"}}},
					{Summary: "b", Fixes: []Ref{{CompareKey: "b"}, {CompareKey: "d"}}},
					{Summary: "c", Fixes: []Ref{{CompareKey: "c"}, {CompareKey: "e"}}},
					{Summary: "d", Fixes: []Ref{{CompareKey: "d"}}},
					{Summary: "e", Fixes: []Ref{{CompareKey: "e"}}},
					{Summary: "f", Fixes: []Ref{{CompareKey: "f"}, {CompareKey: "z"}}},
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				report := Report{
					Remediations: tc.Remediations,
				}

				report.Sort()

				require.Equal(t, tc.Want, report.Remediations)
			})
		}
	})

	t.Run("Dependencies", func(t *testing.T) {
		tcs := []struct {
			Name            string
			DependencyFiles []DependencyFile
			Want            []DependencyFile
		}{
			{
				Name: "it sorts by Dependency[].Package.Name when all Dependency[].Package.Names differ",
				DependencyFiles: []DependencyFile{
					{Dependencies: []Dependency{{Package: Package{Name: "z"}, Version: "1.4.2"}, {Package: Package{Name: "a"}, Version: "7.8.9"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "s"}, Version: "3.4.1"}, {Package: Package{Name: "d"}, Version: "5.7.4"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "e"}, Version: "9.14.2"}, {Package: Package{Name: "d"}, Version: "9.15.3"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "6.1.8"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "t"}, Version: "4.2.5"}}},
					{Dependencies: []Dependency{
						{Package: Package{Name: "g"}, Version: "5.4.5"},
						{Package: Package{Name: "e"}, Version: "7.8.9"},
						{Package: Package{Name: "a"}, Version: "2.88.19"},
					}},
				},
				Want: []DependencyFile{
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "7.8.9"}, {Package: Package{Name: "z"}, Version: "1.4.2"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "d"}, Version: "5.7.4"}, {Package: Package{Name: "s"}, Version: "3.4.1"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "d"}, Version: "9.15.3"}, {Package: Package{Name: "e"}, Version: "9.14.2"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "6.1.8"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "t"}, Version: "4.2.5"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "2.88.19"}, {Package: Package{Name: "e"}, Version: "7.8.9"}, {Package: Package{Name: "g"}, Version: "5.4.5"}}},
				},
			},
			{
				Name: "it sorts by Dependency[].Version when the Dependency[].Package.Names are the same",
				DependencyFiles: []DependencyFile{
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "7.8.9"}, {Package: Package{Name: "a"}, Version: "1.4.2"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "s"}, Version: "3.4.1"}, {Package: Package{Name: "s"}, Version: "5.7.4"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "e"}, Version: "9.14.2"}, {Package: Package{Name: "d"}, Version: "9.15.3"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "6.1.8"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "t"}, Version: "1.1.1"}}},
					{Dependencies: []Dependency{
						{Package: Package{Name: "g"}, Version: "2.88.19"},
						{Package: Package{Name: "g"}, Version: "7.8.9"},
						{Package: Package{Name: "g"}, Version: "5.4.5"},
					}},
				},
				Want: []DependencyFile{
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "1.4.2"}, {Package: Package{Name: "a"}, Version: "7.8.9"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "s"}, Version: "3.4.1"}, {Package: Package{Name: "s"}, Version: "5.7.4"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "d"}, Version: "9.15.3"}, {Package: Package{Name: "e"}, Version: "9.14.2"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "a"}, Version: "6.1.8"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "t"}, Version: "1.1.1"}}},
					{Dependencies: []Dependency{{Package: Package{Name: "g"}, Version: "2.88.19"}, {Package: Package{Name: "g"}, Version: "5.4.5"}, {Package: Package{Name: "g"}, Version: "7.8.9"}}},
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Name, func(t *testing.T) {
				report := Report{
					DependencyFiles: tc.DependencyFiles,
				}

				report.Sort()

				require.Equal(t, tc.Want, report.DependencyFiles)
			})
		}
	})
}

func TestMergeReports(t *testing.T) {
	location := Location{File: "app/Gemfile.lock"}
	ids := []Identifier{CVEIdentifier("CVE-2018-14404")}

	reports := []Report{
		{
			Version: Version{Major: 2, Minor: 3},
			Vulnerabilities: []Vulnerability{
				{
					Name:       "R1/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R1/low/ckey",
				},
				{
					Name:        "R1/critical",
					Severity:    SeverityLevelCritical,
					Confidence:  ConfidenceLevelUnknown,
					Location:    location,
					Identifiers: ids,
					CompareKey:  "R1/critical/ckey",
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R1/critical/ckey"}},
					Summary: "Upgrade dependency to fix R1/critical",
					Diff:    "diff fixing R1/critical",
				},
				{
					Fixes:   []Ref{{CompareKey: "C2/important/ckey"}},
					Summary: "Upgrade dependency to fix C2/important",
					Diff:    "diff fixing C2/important",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
			},
		},
		{
			Version: Version{Major: 2, Minor: 4},
			Vulnerabilities: []Vulnerability{
				{
					Name:       "R2/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/low/ckey",
				},
				{
					Name:       "R2/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R2/critical/ckey",
				},
			},
		},
		{
			Version: Version{Major: 2, Minor: 5},
			Vulnerabilities: []Vulnerability{
				{
					Name:       "R3/low",
					Severity:   SeverityLevelLow,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/low/ckey",
				},
				{
					Name:       "R3/critical",
					Severity:   SeverityLevelCritical,
					Confidence: ConfidenceLevelUnknown,
					CompareKey: "R3/critical/ckey",
				},
				{
					Name:        "R3/critical/dup",
					Severity:    SeverityLevelCritical,
					Confidence:  ConfidenceLevelUnknown,
					Location:    location,
					Identifiers: ids,
					CompareKey:  "R3/critical/ckey",
				},
			},
			Remediations: []Remediation{
				{
					Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
					Summary: "Upgrade dependency to fix R3/critical",
					Diff:    "diff fixing R3/critical",
				},
			},
			DependencyFiles: []DependencyFile{
				{
					Path:           "api/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "org.apache.struts/struts2-core"}, Version: "2.5.1"},
						{Package: Package{Name: "org.freemarker/freemarker"}, Version: "2.3.23"},
						{Package: Package{Name: "ognl/ognl"}, Version: "3.1.8"},
					},
				},
				{
					Path:           "model/pom.xml",
					PackageManager: "maven",
					Dependencies: []Dependency{
						{Package: Package{Name: "org.apache.logging.log4j/log4j-core"}, Version: "2.8.2"},
						{Package: Package{Name: "org.apache.logging.log4j/log4j-api"}, Version: "2.8.2"},
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
				{
					Path:           "web/pom.xml",
					PackageManager: "bundler", // other package manager
					Dependencies: []Dependency{
						{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
						{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
						{Package: Package{Name: "junit/junit"}, Version: "3.8.2"}, // other version
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"}, // duplicate
						{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					},
				},
			},
		},
	}

	want := Report{
		Version: CurrentVersion(),
		Vulnerabilities: []Vulnerability{
			{
				Name:        "R1/critical",
				Severity:    SeverityLevelCritical,
				Confidence:  ConfidenceLevelUnknown,
				Location:    location,
				Identifiers: ids,
				CompareKey:  "R1/critical/ckey",
			},
			{
				Name:       "R2/critical",
				Severity:   SeverityLevelCritical,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R2/critical/ckey",
			},
			{
				Name:       "R3/critical",
				Severity:   SeverityLevelCritical,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R3/critical/ckey",
			},
			{
				Name:       "R1/low",
				Severity:   SeverityLevelLow,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R1/low/ckey",
			},
			{
				Name:       "R2/low",
				Severity:   SeverityLevelLow,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R2/low/ckey",
			},
			{
				Name:       "R3/low",
				Severity:   SeverityLevelLow,
				Confidence: ConfidenceLevelUnknown,
				CompareKey: "R3/low/ckey",
			},
		},
		Remediations: []Remediation{
			{
				Fixes:   []Ref{{CompareKey: "C2/important/ckey"}},
				Summary: "Upgrade dependency to fix C2/important",
				Diff:    "diff fixing C2/important",
			},
			{
				Fixes:   []Ref{{CompareKey: "R1/critical/ckey"}},
				Summary: "Upgrade dependency to fix R1/critical",
				Diff:    "diff fixing R1/critical",
			},
			{
				Fixes:   []Ref{{CompareKey: "R3/critical/ckey"}},
				Summary: "Upgrade dependency to fix R3/critical",
				Diff:    "diff fixing R3/critical",
			},
		},
		DependencyFiles: []DependencyFile{
			{
				Path:           "api/pom.xml",
				PackageManager: "maven",
				Dependencies: []Dependency{
					{Package: Package{Name: "ognl/ognl"}, Version: "3.1.8"},
					{Package: Package{Name: "org.apache.struts/struts2-core"}, Version: "2.5.1"},
					{Package: Package{Name: "org.freemarker/freemarker"}, Version: "2.3.23"},
				},
			},
			{
				Path:           "model/pom.xml",
				PackageManager: "maven",
				Dependencies: []Dependency{
					{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					{Package: Package{Name: "org.apache.logging.log4j/log4j-api"}, Version: "2.8.2"},
					{Package: Package{Name: "org.apache.logging.log4j/log4j-core"}, Version: "2.8.2"},
				},
			},
			{
				Path:           "pom.xml",
				PackageManager: "maven",
				Dependencies: []Dependency{
					{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
				},
			},
			{
				Path:           "web/pom.xml",
				PackageManager: "bundler",
				Dependencies: []Dependency{
					{Package: Package{Name: "io.netty/netty-all"}, Version: "4.1.0.Final"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.1"},
					{Package: Package{Name: "junit/junit"}, Version: "3.8.2"},
					{Package: Package{Name: "org.codehaus.woodstox/stax2-api"}, Version: "3.1.4"},
				},
			},
		},
	}

	got := MergeReports(reports...)
	require.Equal(t, want, got)
}

func TestDisableIdentifierReports(t *testing.T) {
	tests := []struct {
		Name           string
		Report         Report
		rulesetPath    string
		wantIds        []Identifier
		wantPrimaryIds []Identifier
		wantErr        string
	}{
		{
			Name: "Filters both primary and non-primary identifiers",
			Report: Report{
				Vulnerabilities: []Vulnerability{
					{
						Identifiers: []Identifier{
							{
								Type:  "CWE",
								Value: "CWE-1",
							},
							{
								Type:  "CWE",
								Value: "CWE-2",
							},
						},
					},
					{
						Identifiers: []Identifier{
							{
								Type:  "CWE",
								Value: "CWE-3",
							},
							{
								Type:  "CWE",
								Value: "CWE-4",
							},
						},
					},
					{
						Identifiers: []Identifier{
							{
								Type:  "CWE",
								Value: "CWE-5",
							},
							{
								Type:  "CWE",
								Value: "CWE-6",
							},
						},
					},
				},
				Scan: Scan{
					PrimaryIdentifiers: []Identifier{
						{Type: "CWE", Value: "CWE-1"},
						{Type: "CWE", Value: "CWE-3"},
						{Type: "CWE", Value: "CWE-5"},
					},
				},
			},
			rulesetPath:    "testdata/sast-ruleset-disable-ids.toml",
			wantIds:        []Identifier{{Type: "CWE", Value: "CWE-3"}, {Type: "CWE", Value: "CWE-4"}},
			wantPrimaryIds: []Identifier{{Type: "CWE", Value: "CWE-3"}},
		},
		{
			Name:        "Bad paths do not drop identifiers",
			Report:      testReport(),
			rulesetPath: "testdata/bad-path.toml",
			wantIds: []Identifier{
				{Type: "cve", Name: "CVE-2018-1234", Value: "CVE-2018-1234", URL: "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234"},
			},
		},
		{
			Name: "Empty rulesets are handled gracefully",
			Report: Report{
				Vulnerabilities: []Vulnerability{},
			},
			rulesetPath: "testdata/sast-ruleset.toml",
			wantIds:     []Identifier{},
		},
	}

	for _, test := range tests {
		// Enable custom ruleset feature availability
		t.Setenv(ruleset.EnvVarGitlabFeatures, ruleset.GitlabFeatureCustomRulesetsSAST)

		t.Run(test.Name, func(t *testing.T) {
			test.Report.FilterDisabledRules(test.rulesetPath, "gosec")

			for _, v := range test.Report.Vulnerabilities {
				require.Equal(t, test.wantIds, v.Identifiers)
			}

			if len(test.wantPrimaryIds) > 0 {
				require.Equal(t, test.wantPrimaryIds, test.Report.Scan.PrimaryIdentifiers)
			}

			// Note: additional check for bug described
			// https://gitlab.com/gitlab-org/security-products/analyzers/common/-/merge_requests/131
			require.NotNil(t, test.Report.Vulnerabilities)
		})
	}
}

func TestApplyOverrides(t *testing.T) {
	// Enable custom ruleset feature availability
	t.Setenv(ruleset.EnvVarGitlabFeatures, ruleset.GitlabFeatureCustomRulesetsSAST)

	t.Run("ApplyReportOverrides", func(t *testing.T) {
		report := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Vulnerability{
				{
					Name:        "Original name",
					Message:     "Original message",
					Description: "Original description",
					Severity:    SeverityLevelMedium,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-79",
						},
					},
				},
				{
					Name:        "Original name",
					Message:     "Original message",
					Description: "Original description",
					Severity:    SeverityLevelMedium,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-1",
						},
					},
				},
				{
					Name:        "Original name",
					Message:     "Original message",
					Description: "Original description",
					Severity:    SeverityLevelMedium,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-99",
						},
					},
				},
				{
					Name:        "Original name",
					Message:     "Original message",
					Description: "Original description",
					Severity:    SeverityLevelMedium,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-100",
						},
					},
				},
			},
		}

		want := Report{
			Version: CurrentVersion(),
			Vulnerabilities: []Vulnerability{
				{
					Name:        "OVERRIDDEN name",
					Message:     "OVERRIDDEN message",
					Description: "OVERRIDDEN description",
					Severity:    SeverityLevelCritical,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-79",
						},
					},
				},
				{
					Name:        "Original name",
					Message:     "Original message",
					Description: "Original description",
					Severity:    SeverityLevelUnknown,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-1",
						},
					},
				},
				{
					Name:        "Original name",
					Message:     "Original message",
					Description: "Original description",
					Severity:    SeverityLevelMedium,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-99",
						},
					},
				},
				{
					Name:        "Original name",
					Message:     "Original message",
					Description: "Original description",
					Severity:    SeverityLevelMedium,
					Scanner: &Scanner{
						ID:   "gosec",
						Name: "Gosec",
					},
					Identifiers: []Identifier{
						{
							Type:  "CWE",
							Value: "CWE-100",
						},
					},
				},
			},
		}

		report.ApplyReportOverrides("testdata/sast-ruleset-overrides.toml", "gosec")
		require.Equal(t, want, report)
	})
}
