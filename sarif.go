package report

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

// Sarif is a standard SAST output format, see https://sarifweb.azurewebsites.net,
// current support for Sarif is limited to mapping GitLab SAST Report to Sarif
type Sarif struct {
	Schema  string     `json:"$schema"`
	Version string     `json:"version"`
	Runs    []SarifRun `json:"runs"`
}

// SarifRun is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifRun struct {
	Tool        SarifTool         `json:"tool"`
	Invocations []SarifInvocation `json:"invocations"`
	Results     []SarifResult     `json:"results"`
}

// SarifTool is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifTool struct {
	Driver SarifDriver `json:"driver"`
}

// SarifDriver is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifDriver struct {
	Name            string      `json:"name"`
	SemanticVersion string      `json:"semanticVersion"`
	InformationURI  string      `json:"informationUri,omitempty"`
	Rules           []SarifRule `json:"rules,omitempty"`
}

// SarifInvocation is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifInvocation struct {
	ToolExecutionNotifications []SarifNotification `json:"toolExecutionNotifications,omitempty"`
	StartTimeUTC               *time.Time          `json:"startTimeUtc,omitempty"`
	EndTimeUTC                 *time.Time          `json:"endTimeUtc,omitempty"`
	ExecutionSuccessful        bool                `json:"executionSuccessful,omitempty"`
}

// SarifNotification is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifNotification struct {
	Descriptor struct {
		ID string `json:"id"`
	} `json:"descriptor"`
	Level   string `json:"level"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
}

// SarifRule is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifRule struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	ShortDescription struct {
		Text string `json:"text"`
	} `json:"shortDescription"`
	FullDescription struct {
		Text string `json:"text"`
	} `json:"fullDescription"`
	DefaultConfiguration struct {
		Level string `json:"level"`
	} `json:"defaultConfiguration"`
	Properties SarifRuleProperties `json:"properties"`
	HelpURI    string              `json:"helpUri"`
}

// SarifRuleProperties is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifRuleProperties struct {
	Precision        string   `json:"precision"`
	Tags             []string `json:"tags"`
	SecuritySeverity string   `json:"security-severity"`
}

// SarifResult is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifResult struct {
	GUID         string          `json:"guid,omitempty"`
	RuleID       string          `json:"ruleId"`
	Level        string          `json:"level,omitempty"`
	Message      SarifMessage    `json:"message"`
	Locations    []SarifLocation `json:"locations"`
	Properties   SarifProperties `json:"properties,omitempty"`
	Suppressions []struct {
		Kind   string `json:"kind"`             // values= 'inSource', 'external'
		Status string `json:"status,omitempty"` // values= empty,'accepted','underReview','rejected'
		GUID   string `json:"guid,omitempty"`
	} `json:"suppressions,omitempty"`
}

// SarifProperties is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifProperties struct {
	Confidence  ConfidenceLevel `json:"confidence,omitempty"`
	Identifiers []Identifier    `json:"identifiers,omitempty"`
}

// SarifMessage is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifMessage struct {
	Text string `json:"text"`
}

// SarifLocation is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifLocation struct {
	PhysicalLocation SarifPhysicalLocation `json:"physicalLocation"`
}

// SarifPhysicalLocation is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifPhysicalLocation struct {
	ArtifactLocation SarifArtifactLocation `json:"artifactLocation"`
	Region           SarifRegion           `json:"region"`
}

// SarifArtifactLocation is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifArtifactLocation struct {
	URI       string `json:"uri"`
	URIBaseID string `json:"uriBaseId,omitempty"`
}

// SarifRegion is a subfield of Sarif specified by https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html
// The prefix 'Sarif' is to avoid name conflict in this project
type SarifRegion struct {
	StartLine   int `json:"startLine,omitempty"`
	StartColumn int `json:"startColumn,omitempty"`
	EndLine     int `json:"endLine,omitempty"`
	EndColumn   int `json:"endColumn,omitempty"`
}

// match CWE-XXX only
var cweIDRegex = regexp.MustCompile(`([cC][wW][eE])-(\d{1,4})`)

// match (TYPE)-(ID): (Description)
var tagIDRegex = regexp.MustCompile(`([^-]+)-([^:]+):\s*(.+)`)

// TransformToGLSASTReport will take in a sarif file and output a GitLab SAST Report
func TransformToGLSASTReport(reader io.Reader, rootPath, analyzerID string, scanner Scanner) (*Report, error) {
	s := Sarif{}

	jsonBytes, err := readerToBytes(reader)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(jsonBytes, &s)
	if err != nil {
		return nil, err
	}

	if s.Version != "2.1.0" {
		return nil, fmt.Errorf("version for SARIF is %s, but we only support 2.1.0", s.Version)
	}

	newReport := NewReport()
	var allVulns []Vulnerability
	allIds := make([]Identifier, 0, len(s.Runs[0].Tool.Driver.Rules))

	idMap := make(map[string]Identifier)

	// It is generally expected to only have a single run, but best to parse all as it is a collection.
	for _, run := range s.Runs {
		for _, invocation := range run.Invocations {
			for _, notification := range invocation.ToolExecutionNotifications {
				logNotification(notification)
			}
		}

		vulns, ruleMap, err := transformRun(run, rootPath, scanner)
		if err != nil {
			return nil, err
		}

		allVulns = append(allVulns, vulns...)

		if sastFPReductionFeatEnabled() {
			for _, rule := range ruleMap {
				idMap[rule.ID] = primaryIdentifier(rule, scanner)
			}
		}
	}

	for _, id := range idMap {
		allIds = append(allIds, id)
	}

	newReport.Analyzer = analyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = allVulns

	if sastFPReductionFeatEnabled() {
		sort.Slice(allIds, func(i, j int) bool {
			return allIds[i].Value < allIds[j].Value
		})
		newReport.Scan.PrimaryIdentifiers = allIds
	}

	return &newReport, nil
}

func readerToBytes(reader io.Reader) ([]byte, error) {
	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(reader)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// shouldSuppress indicates if the given finding(`result`) should be included
// in the artifact. This is determined based on the `suppressions` property from
// the sarif output file.
func shouldSuppress(r SarifResult) bool {
	if len(r.Suppressions) == 0 {
		return false
	}
	for _, sup := range r.Suppressions {
		// if one of the suppressions is under review or rejected, include
		// the finding in the artifact.
		if sup.Status == "underReview" || sup.Status == "rejected" {
			return false
		}
	}
	return true
}

func transformRun(r SarifRun, rootPath string, scanner Scanner) ([]Vulnerability, map[string]SarifRule, error) {
	ruleMap := make(map[string]SarifRule)
	for _, rule := range r.Tool.Driver.Rules {
		ruleMap[rule.ID] = rule
	}

	var vulns []Vulnerability
	for _, result := range r.Results {
		if shouldSuppress(result) {
			continue
		}
		for _, location := range result.Locations {
			rule := ruleMap[result.RuleID]

			var description string
			if len(result.Message.Text) > VulnerabilityDescriptionMaxLengthBytes {
				description = result.Message.Text[:VulnerabilityDescriptionMaxLengthBytes]
			} else {
				description = result.Message.Text
			}

			lineEnd := location.PhysicalLocation.Region.EndLine

			vulns = append(vulns, Vulnerability{
				Description: description,
				Category:    CategorySast,
				Name:        name(rule, scanner.Name),
				Severity:    severity(rule),
				Scanner:     &scanner,
				Location: Location{
					File:      removeRootPath(location.PhysicalLocation.ArtifactLocation.URI, rootPath),
					LineStart: location.PhysicalLocation.Region.StartLine,
					LineEnd:   lineEnd,
				},
				Identifiers: identifiers(rule, scanner),
			})
		}
	}
	return vulns, ruleMap, nil
}

// See: https://docs.oasis-open.org/sarif/sarif/v2.1.0/os/sarif-v2.1.0-os.html#_Toc34317855 for more
// information about the level property. The docs say that when level is not defined, then the value is equal
// to warning.
func severity(r SarifRule) SeverityLevel {
	// if `security-severity` is available, parse it instead of the `DefaultConfiguration` property
	if r.Properties.SecuritySeverity != "" {
		parsedSeverity := ParseSeverityLevel(r.Properties.SecuritySeverity)
		securitySeverity := strings.TrimSpace(strings.ToLower(r.Properties.SecuritySeverity))

		if parsedSeverity != SeverityLevelUnknown {
			return parsedSeverity
		}

		// parsed security-severity returns unknown, check if it's actually unknown, or just invalid
		if strings.EqualFold(securitySeverity, "unknown") {
			return SeverityLevelUnknown
		}
	}

	// security-severity was invalid or not provided, fall through to the `DefaultConfiguration` check
	switch r.DefaultConfiguration.Level {
	case "error":
		return SeverityLevelCritical
	case "warning":
		return SeverityLevelMedium
	case "note":
		return SeverityLevelInfo
	case "none":
		// This primarily caters for Kics, which tends to map "info" level rules to the "none" SARIF level. This
		// causes a loss in fidelity when we transform the SARIF report into a GL SAST report, resulting in "info"
		// level findings being reported as "unknown" in the repository's Vulnerability Report.
		//
		// By mapping back to info, we're essentially un-doing the logic in Kics.
		//
		// See https://gitlab.com/gitlab-org/gitlab/-/issues/349141#note_1145792336 for details.
		return SeverityLevelInfo
	default:
		return SeverityLevelMedium
	}
}

func name(r SarifRule, scannerName string) string {
	// KiCS (for now) prefers the fullDescription field as shortDescription is used for the identifier
	if strings.EqualFold(strings.ToLower(scannerName), "kics") {
		if len(r.FullDescription.Text) > VulnerabilityNameMaxLengthBytes {
			return r.FullDescription.Text[:VulnerabilityNameMaxLengthBytes-3] + "..."
		}

		return r.FullDescription.Text
	}

	// prefer shortDescription field over tag: CWE: <text> for semgrep
	if r.ShortDescription.Text != "" && !strings.EqualFold(r.FullDescription.Text, r.ShortDescription.Text) {
		if len(r.ShortDescription.Text) > VulnerabilityNameMaxLengthBytes {
			return r.ShortDescription.Text[:VulnerabilityNameMaxLengthBytes-3] + "..."
		}

		return r.ShortDescription.Text
	}

	for _, tag := range r.Properties.Tags {
		splits := strings.Split(tag, ":")
		if strings.HasPrefix(splits[0], "CWE") && len(splits) > 1 {
			return strings.TrimLeft(splits[1], " ")
		}
	}

	// default to full text description
	if len(r.FullDescription.Text) > VulnerabilityNameMaxLengthBytes {
		return r.FullDescription.Text[:VulnerabilityNameMaxLengthBytes-3] + "..."
	}

	return r.FullDescription.Text
}

func identifiers(r SarifRule, scanner Scanner) []Identifier {
	ids := []Identifier{
		primaryIdentifier(r, scanner),
	}

	for _, tag := range r.Properties.Tags {

		matches := findTagMatches(tag)
		if matches == nil {
			continue
		}

		switch strings.ToLower(matches[1]) {
		case "cwe":
			cweID, err := strconv.Atoi(matches[2])
			if err != nil {
				log.Errorf("Failure to parse CWE ID: %v\n", err)
				continue
			}

			ids = append(ids, CWEIdentifier(cweID))
		case "owasp":
			id := matches[2]
			desc := matches[3]

			components := strings.SplitN(matches[3], "-", 2)
			if len(components) == 2 {
				year := strings.TrimSpace(components[0])
				if _, err := strconv.Atoi(year); err == nil {
					id = id + ":" + year
					desc = strings.TrimSpace(components[1])
				}
			}

			ids = append(ids, OWASPTop10Identifier(id, desc))
		default:
			ids = append(ids, Identifier{
				Type:  IdentifierType(strings.ToLower(matches[1])),
				Name:  matches[3],
				Value: matches[2],
			})
		}
	}

	return ids
}

func findTagMatches(tag string) []string {
	// first try to extract (TAG)-(ID): (Description)
	matches := tagIDRegex.FindStringSubmatch(tag)

	if matches == nil {
		// see if we just have a CWE-(XXX) tag
		matches = cweIDRegex.FindStringSubmatch(tag)
	}

	return matches
}

func primaryIdentifier(r SarifRule, scanner Scanner) Identifier {
	return Identifier{
		Type:  IdentifierType(fmt.Sprintf("%s_id", scanner.ID)),
		Name:  r.Name,
		Value: r.ID,
		URL:   r.HelpURI,
	}
}

func removeRootPath(path, rootPath string) string {
	prefix := strings.TrimSuffix(rootPath, "/") + "/"
	if path[0] != '/' {
		prefix = strings.TrimPrefix(prefix, "/")
	}

	return strings.TrimPrefix(path, prefix)
}

func notificationMsg(notification SarifNotification) string {
	return fmt.Sprintf(
		"tool notification %s: %s %s",
		notification.Level,
		notification.Descriptor.ID,
		notification.Message.Text)
}

// https://docs.oasis-open.org/sarif/sarif/v2.1.0/csprd01/sarif-v2.1.0-csprd01.html#_Ref493404972
func logNotification(notification SarifNotification) {
	msg := notificationMsg(notification)
	switch notification.Level {
	case "error":
		log.Error(msg)
	case "warning":
		log.Warn(msg)
	case "note":
		log.Info(msg)
	case "none":
		log.Debug(msg)
	default:
		log.Debug(msg)
	}
}

// sastFPReductionFeatEnabled returns true if "sast_fp_reduction" feature flag
// is present in the "GITLAB_FEATURES" env variable
func sastFPReductionFeatEnabled() bool {
	features := os.Getenv("GITLAB_FEATURES")
	return strings.Contains(features, "sast_fp_reduction")
}

// See: https://docs.oasis-open.org/sarif/sarif/v2.1.0/os/sarif-v2.1.0-os.html#_Toc34317855
// and https://docs.gitlab.com/ee/user/application_security/vulnerabilities/severities.html
// for more information about the level property.
func toSarifSeverity(v Vulnerability) string {
	switch v.Severity {
	case SeverityLevelCritical, SeverityLevelHigh:
		return "error"
	case SeverityLevelMedium:

		return "warning"
	case SeverityLevelLow, SeverityLevelInfo:
		return "note"
	default: // report.SeverityLevelUnknown, report.SeverityLevelUndefined:
		return "none"
	}
}

// TransformGLSASTFileToSarif takes gl-sast-report.json as input and exports sarif format
func TransformGLSASTFileToSarif(reader io.Reader) (*Sarif, error) {
	jsonBytes, err := readerToBytes(reader)
	if err != nil {
		return nil, err
	}

	var report Report
	err = json.Unmarshal(jsonBytes, &report)
	if err != nil {
		return nil, err
	}

	return TransformReportToSarif(&report), nil
}

// TransformReportToSarif takes Report as input and exports sarif format
func TransformReportToSarif(report *Report) *Sarif {
	var results = make([]SarifResult, 0, len(report.Vulnerabilities))
	for _, vuln := range report.Vulnerabilities {
		item := SarifResult{
			RuleID:  vuln.Name,
			GUID:    vuln.ID(),
			Level:   toSarifSeverity(vuln),
			Message: SarifMessage{vuln.Description},
			Locations: []SarifLocation{{
				PhysicalLocation: SarifPhysicalLocation{
					ArtifactLocation: SarifArtifactLocation{
						URI: vuln.Location.File,
					},
					Region: SarifRegion{
						StartLine: vuln.Location.LineStart,
						EndLine:   vuln.Location.LineEnd,
					},
				}},
			},
			Properties: SarifProperties{
				Confidence:  vuln.Confidence,
				Identifiers: vuln.Identifiers,
			},
		}
		results = append(results, item)
	}

	return &Sarif{
		Version: "2.1.0",
		Schema:  "http://json.schemastore.org/sarif-2.1.0-rtm.4",
		Runs: []SarifRun{{
			Invocations: []SarifInvocation{{
				ExecutionSuccessful: report.Scan.Status == StatusSuccess,
				StartTimeUTC:        (*time.Time)(report.Scan.StartTime),
				EndTimeUTC:          (*time.Time)(report.Scan.EndTime),
			}},
			Results: results,
			Tool: SarifTool{
				Driver: SarifDriver{
					Name:            report.Scan.Scanner.Name,
					SemanticVersion: report.Scan.Scanner.Version,
					InformationURI:  report.Scan.Scanner.URL,
				},
			},
		}},
	}
}
